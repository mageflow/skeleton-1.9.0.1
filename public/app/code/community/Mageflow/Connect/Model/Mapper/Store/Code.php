<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Code.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Model_Mapper_Store_Code
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_Model_Mapper_Store_Code extends Mageflow_Connect_Model_Mapper_Base
{

    /**
     * Search for store names by ID-s and replace ID-s with names for better interoperability
     *
     * @param $fromValue
     * @param object $context
     * @return mixed|void
     */
    public function mapValue($fromValue, $context)
    {
        $this->log($fromValue);

        $toValue = array();
        if (null === $fromValue) {
            $storeIdArr = $context->getResource()->lookupStoreIds($context->getId());
            $fromValue = $storeIdArr;
        }
        if (is_scalar($fromValue)) {
            /**
             * @var Mage_Core_Model_Store $store
             */
            $store = Mage::getModel('core/store')->load($fromValue);
            $toValue[] = $store->getName();
        } elseif (is_array($fromValue)) {
            foreach ($fromValue as $storeId) {
                /**
                 * @var Mage_Core_Model_Store $store
                 */
                $store = Mage::getModel('core/store')->load($storeId);
                $toValue[] = $store->getName();
            }
        }
        $this->log($toValue);

        return parent::mapValue($toValue, $context);
    }
} 