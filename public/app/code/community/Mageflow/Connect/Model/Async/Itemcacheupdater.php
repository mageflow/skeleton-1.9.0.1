<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Itemcacheupdarer.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Model_Async_Itemcacheupdater
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 *
 */
class Mageflow_Connect_Model_Async_Itemcacheupdater extends Mageflow_Connect_Model_Abstract
{

    /**
     * Public interface to cron functions is run()
     */
    public function run()
    {
        /**
         * @var Mageflow_Connect_Helper_Oauth $oauthHelper
         */
        $oauthHelper = Mage::helper('mageflow_connect/oauth');
        /**
         * Delete existing items in cache table
         */
        Mage::getModel('mageflow_connect/changeset_item_cache')->getResource()->truncate();

        $response = $oauthHelper->getApiClient()->get('changesetitem',
            array(
                'days_back' => Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_PULL_DAYS_BACK),
                'project' => Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_PROJECT),
                'instance_key' => Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_INSTANCE_KEY),
            )
        );
        $itemCollection = json_decode($response);
        if (is_object($itemCollection)) {

            foreach ($itemCollection->items as $item) {
                $data['id'] = $item->id;

                $this->log(sprintf('Caching changeset item with ID=%s', $item->id));

                /**
                 * @var Mageflow_Connect_Model_Changeset_Item_Cache $model
                 */
                $mfGuid = null;
                if (null !== $item->meta_info) {
                    $metaInfo = json_decode($item->meta_info);
                    if (isset($metaInfo->mf_guid)) {
                        $mfGuid = $metaInfo->mf_guid;
                    }
                }
                $now = new Zend_Date();
                $model = Mage::getModel('mageflow_connect/changeset_item_cache');
                $model->setRemoteId($item->id);
                $model->setDescription($item->description);
                $model->setContent($item->content);
                $model->setMetaInfo($item->meta_info);
                $model->setType($item->type);
                $model->setMfGuid($mfGuid);
                $model->setCreatedAt($item->created_at);
                $model->setUpdatedAt($now);
                $model->setCreatedBy($item->created_by);
                $model->save();
            }
            return true;
        }
        return false;
    }
} 