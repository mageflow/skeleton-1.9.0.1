<?php


/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Generic.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Helper
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Model_Handler_Generic
 *
 * Generic data handler for these cases where specific handler is not created yet. It helps
 * to avoid null exceptions. It logs a message for each non-implemented case
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Helper
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 *
 */
class Mageflow_Connect_Model_Handler_Generic extends Mageflow_Connect_Model_Handler_Abstract
{

    /**
     * Data processing is process where data is being sent into API and
     * MFX creates a Magento entity based on that data
     * @param array $data
     * @return array
     */
    public function processData(array $data)
    {
        $this->log('GENERIC data handler used for processing. Check implementation!');
        return array();
    }

    /**
     * Data packing is process where Magento entity is packed into portable
     * JSON container
     *
     * @param Mage_Core_Model_Abstract $data
     * @return array
     */
    public function packData(Mage_Core_Model_Abstract $data)
    {
        $this->log('GENERIC data handler used for packing. Check implementation!');
        return array();
    }


} 