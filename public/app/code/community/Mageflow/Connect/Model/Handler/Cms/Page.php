<?php
/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Page.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Helper
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Model_Handler_Cms_Page
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Helper
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_Model_Handler_Cms_Page
    extends Mageflow_Connect_Model_Handler_Cms_Abstract
{
    /**
     * update or create cms/page from data array
     *
     * @param array $data
     *
     * @return array
     * @throws Exception
     */
    public function processData(array $data)
    {
        $data = isset($data[0]) ? $data[0] : $data;
        $savedEntity = null;
        $message = null;

        if (isset($data['stores']) && is_array($data['stores']) && count($data['stores'])) {
            $storeIdList = $this->getStoreIdListByCodes($data['stores']);
            if ($storeIdList == array()) {
                throw new Exception('no matching stores');
            }
            if (count($data['stores']) != count($storeIdList)) {
                $message =
                    "Notice: following store views are missing from target: "
                    . $this->getMissingStores($data['stores']);
            }
            $data['stores'] = $storeIdList;
        } else {
            $data['stores'] = array();
        }

        /**
         * @var Mage_Cms_Model_Page $model
         */
        $model = $this->findModel('cms/page', $data['mf_guid'], array('field' => 'identifier', 'value' => $data['identifier'], 'stores' => $data['stores']));
        $data['page_id'] = $model->getPageId();


        try {
            $savedEntity = $this->saveItem($model, $data);
        } catch (Exception $ex) {
            $message = $ex->getMessage();
            $this->log($ex->getMessage());
            $this->log($ex->getTraceAsString());
        }

        return $this->sendProcessingResponse($savedEntity, $message);
    }
}