<?php
/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Media.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Helper
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Model_Handler_Cms_Media
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Helper
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_Model_Handler_Cms_Media
    extends Mageflow_Connect_Model_Handler_Abstract
{

    /**
     * find item
     *
     * @param $mfGuid
     *
     * @return Mageflow_Connect_Model_Media_Index
     */
    private function findItem($mfGuid)
    {
        $collection = $this->getWorkingModel()->getCollection();
        $collection->addFilter('mf_guid', $mfGuid);
        $collection->load();
        if ($collection->getFirstItem() instanceof
            Mageflow_Connect_Model_Media_Index
            && $collection->getFirstItem()->getId() > 0
        ) {
            return $collection->getFirstItem();
        }
        return null;
    }

    /**
     * Create changeset item from Mageflow_Connect_Model_Media_Index
     *
     * @param $model
     *
     * @return array|void
     */
    public function packData(Mage_Core_Model_Abstract $model)
    {
        $this->log(print_r($model, true));
        $model['hex'] = bin2hex(file_get_contents($model['filename']));
        return $model;
    }

    /**
     * update or create CMS Media
     *
     * @param $data
     *
     * @return array
     */
    public function processData(array $data)
    {
        $model = $this->findModel('mageflow_connect/media_index', $data['mf_guid']);

        $filePath
            = Mage::getBaseDir('base') . '/' . ltrim($model->getPath(), '/');
        $this->log(
            'Saving file to ' . $filePath
        );
        $dirPath = dirname($filePath);
        if (!file_exists($dirPath)) {
            @mkdir($dirPath, 0777, true);
            $this->logPhpError(error_get_last());
        }
        @file_put_contents($filePath, pack('H*', $data['hex']));
        $this->logPhpError(error_get_last());

        $model->setMtime(time());
        $model->setSize(filesize($filePath));
        $model->save();
        $this->log($model);
        $this->_successMessage(
            'Created media file',
            200,
            $this->packItem($model)
        );

        return $this->sendProcessingResponse($model);
    }

    /**
     * @param Mageflow_Connect_Model_Interfaces_Changeitem $row
     * @return string
     */
    public function getPreview(Mageflow_Connect_Model_Interfaces_Changeitem $row)
    {
        $output = '';
        $content = json_decode($row->getContent());
        if (null !== $content->basename) {
            $output = sprintf(
                "%s (%s KB)", $content->basename,
                round(filesize($content->filename) / 1024)
            );
        }
        return $output;
    }
}