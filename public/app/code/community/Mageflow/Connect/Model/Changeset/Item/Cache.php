<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * Cache.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Model_Changeset_Item_Cache
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 *
 * @method setRemoteId($value)
 * @method setType($value)
 * @method setDescription($value)
 * @method setMetaInfo($value)
 * @method setContent($value)
 * @method setCreatedAt($value)
 * @method setUpdatedAt($value)
 * @method setMfGuid($value)
 *
 */
class Mageflow_Connect_Model_Changeset_Item_Cache extends Mage_Core_Model_Abstract implements Mageflow_Connect_Model_Interfaces_Changeitem
{

    /**
     * Class constructor
     */
    public function _construct()
    {
        $this->_init('mageflow_connect/changeset_item_cache');
    }

    /**
     * Finds last cache update timestamp
     * @return Zend_Date
     */
    public function getLastUpdated()
    {
        /**
         * @var Mageflow_Connect_Model_Resource_Changeset_Item_Cache_Collection $collection
         */
        $collection = $this->getCollection();
        $collection->addOrder('updated_at', 'DESC');
        $model = $collection->load()->getFirstItem();
        if ($model instanceof Mageflow_Connect_Model_Changeset_Item_Cache && $model->getId() > 0) {
            $updatedAt = $model->getUpdatedAt();
            $dateObject = new Zend_Date($updatedAt, 'YYYY-MM-dd HH:mm:ss');
            return $dateObject;
        }
        $date = new Zend_Date();
        $date->sub(Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_PULL_DAYS_BACK), Zend_Date::DAY);
        return $date;
    }

    /**
     * Returns MFGUID
     * @return string
     */
    public function getMfGuid()
    {
        return $this->getData('mf_guid');
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->getData('content');
    }
}
