<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * V1.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Model_Api2_System_Rewrite_Rest_Admin_V1
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_Model_Api2_System_Rewrite_Rest_Admin_V1
    extends Mageflow_Connect_Model_Api2_Abstract
{
    /**
     * resource type
     *
     * @var string
     */
    protected $_resourceType = 'system_configuration';


    /**
     * Returns array with system info
     *
     * @return array
     */
    public function _retrieve()
    {
        $this->log($this->getRequest()->getParams());
        $classInfoNodeList = Mage::getConfig()->getNode()->xpath(
            '//global//rewrite/..'
        );
        $outItems = array();

        foreach ($classInfoNodeList as $classInfoNode) {
            $rewrite = $classInfoNode->xpath('rewrite');
            if (is_array($rewrite) && sizeof($rewrite) > 0) {
                $keys = array_keys($rewrite[0]->asArray());
                $classSuffix = $keys[0];
                $rewriteClass = (string)$classInfoNode->rewrite->$classSuffix;
                $className = $classInfoNode->class . '_' . uc_words(
                        $classSuffix,
                        '_'
                    );
                $outItem = array(
                    'original' => $className,
                    'rewriter' => $rewriteClass
                );
                $outItems[] = $outItem;
            }
        }
        $this->log($outItems);
        return $outItems;
    }

    /**
     * retrieve collection
     *
     * @return array
     */
    public function _retrieveCollection()
    {
        return $this->_retrieve();
    }


}