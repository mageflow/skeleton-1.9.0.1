<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * V1.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_Model_Api2_System_Configuration_Rest_Admin_V1
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Model
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_Model_Api2_System_Configuration_Rest_Admin_V1
    extends Mageflow_Connect_Model_Api2_Abstract
{
    /**
     * resource type
     *
     * @var string
     */
    protected $_resourceType = 'system_configuration';

    /**
     * Returns array with system info
     *
     * @return array
     */
    public function _retrieve()
    {
        $out = array();
        $resourceModel = $this->getWorkingModel();
        /**
         * @var Mage_Core_Model_Resource_Config_Data_Collection $itemCollection
         */
        $itemCollection = $resourceModel->getCollection();
        $path = $this->getRequest()->getParam('path', null);
        if (!is_null($path)) {
            $path = str_replace(
                ':',
                '/',
                $path
            );

            $itemCollection->addFieldToFilter(array('mf_guid','path'),
                array(
                    array(
                        'eq'=>$path
                    ),
                    array(
                        'eq'=>$path
                    )
                )
            );
        }
        $scopeId = $this->getRequest()->getParam('scope_id', null);
        if (!is_null($scopeId)) {
            $itemCollection->addFieldToFilter('scope_id', $scopeId);
        }
        $configId = $this->getRequest()->getParam('id', null);
        if (!is_null($configId)) {
            $this->log($configId);
            $itemCollection->addFieldToFilter('config_id', $configId);
        }

        $out = $this->packModelCollection($itemCollection);

        $this->log($out);
        return $out;
    }

    /**
     * retrieve collection
     *
     * @return array
     */
    public function _retrieveCollection()
    {
        return $this->_retrieve();
    }


}
