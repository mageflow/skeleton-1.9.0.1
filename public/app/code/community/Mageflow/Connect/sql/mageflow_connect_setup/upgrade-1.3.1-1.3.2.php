<?php
/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * upgrade-1.3.1-1.3.2.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Sql Install & Upgrade
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

$isDevMode = Mage::getIsDeveloperMode();
Mage::setIsDeveloperMode(true);

/* @var Mageflow_Connect_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

$tableName = 'mageflow_connect/changeset_item';

$table = $installer->getTable($tableName);

$connection = $installer->getConnection();
if ($table && $connection->isTableExists($table)) {

    $columnName = 'created_by';
    if (!$connection->tableColumnExists($table, $columnName)) {
        $connection->addColumn(
            $table,
            $columnName,
            'VARCHAR(255)'
        );
    }

    $keyNameList = array();
    foreach ($installer->getConnection()->getIndexList($table) as $index) {
        $keyNameList[] = $index['KEY_NAME'];
    }
    $indexName = 'ix_' . $columnName;
    if (!in_array($indexName, array_values($keyNameList))) {
        $installer->getConnection()->addIndex(
            $table,
            'ix_' . $columnName,
            array($columnName),
            Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX
        );
    }
}

$installer->endSetup();

Mage::setIsDeveloperMode($isDevMode);