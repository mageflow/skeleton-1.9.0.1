<?xml version="1.0" encoding="utf-8"?>
<!--
/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * MageFlow Connector Magento extension configuration file
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Configuration
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
-->
<config>
    <modules>
        <Mageflow_Connect>
            <version>1.4.7</version>
            <name>MageFlow Connector</name>
        </Mageflow_Connect>
    </modules>
    <global>
        <models>
            <mageflow_connect>
                <class>Mageflow_Connect_Model</class>
                <resourceModel>mageflow_connect_resource</resourceModel>
            </mageflow_connect>
            <mageflow_connect_resource>
                <class>Mageflow_Connect_Model_Resource</class>
                <entities>
                    <performance_history>
                        <table>mageflow_performance_history</table>
                    </performance_history>
                    <changeset_item>
                        <table>mageflow_changeset_item</table>
                    </changeset_item>
                    <media_index>
                        <table>mageflow_media_index</table>
                    </media_index>
                    <changeset_item_cache>
                        <table>mageflow_changeset_item_cache</table>
                    </changeset_item_cache>
                </entities>
            </mageflow_connect_resource>
            <core_resource>
                <rewrite>
                    <config_data>Mageflow_Connect_Model_Resource_Config_Data</config_data>
                </rewrite>
            </core_resource>
            <admin>
                <rewrite>
                    <observer>Mageflow_Connect_Model_Admin_Observer</observer>
                </rewrite>
            </admin>
            <api2>
                <rewrite>
                    <acl_filter>Mageflow_Connect_Model_Api2_Acl_Filter</acl_filter>
                </rewrite>
            </api2>
        </models>
        <resources>
            <mageflow_connect_setup>
                <setup>
                    <module>Mageflow_Connect</module>
                    <class>Mageflow_Connect_Model_Resource_Setup</class>
                </setup>
            </mageflow_connect_setup>
        </resources>
        <helpers>
            <mageflow_connect>
                <class>Mageflow_Connect_Helper</class>
            </mageflow_connect>
        </helpers>
        <blocks>
            <mageflow_connect>
                <class>Mageflow_Connect_Block</class>
            </mageflow_connect>
        </blocks>
        <events>
            <save_changeset_item>
                <observers>
                    <on_save_changeset_item>
                        <class>mageflow_connect/observer</class>
                        <method>onSaveChangesetItem</method>
                    </on_save_changeset_item>
                </observers>
            </save_changeset_item>
            <controller_front_send_response_after>
                <observers>
                    <collect_memory_usage>
                        <class>mageflow_connect/observer</class>
                        <method>collectMemoryUsage</method>
                    </collect_memory_usage>
                </observers>
            </controller_front_send_response_after>
            <model_save_commit_after>
                <observers>
                    <on_model_save_commit_after>
                        <class>mageflow_connect/observer</class>
                        <method>onModelSaveCommitAfter</method>
                    </on_model_save_commit_after>
                </observers>
            </model_save_commit_after>
            <category_move>
                <observers>
                    <on_move_category>
                        <class>mageflow_connect/observer</class>
                        <method>onMoveCategory</method>
                    </on_move_category>
                </observers>
            </category_move>
            <model_save_before>
                <observers>
                    <add_mf_data>
                        <class>mageflow_connect/observer</class>
                        <method>onBeforeSave</method>
                    </add_mf_data>
                </observers>
            </model_save_before>
            <model_config_data_save_before>
                <observers>
                    <add_mf_data>
                        <class>mageflow_connect/observer</class>
                        <method>onBeforeSave</method>
                    </add_mf_data>
                </observers>
            </model_config_data_save_before>
            <controller_action_predispatch>
                <observers>
                    <mageflow_controller_action_predispatch>
                        <class>mageflow_connect/observer</class>
                        <method>onControllerActionPredispatch</method>
                    </mageflow_controller_action_predispatch>
                </observers>
            </controller_action_predispatch>
        </events>
        <api2>
            <response>
                <renders>
                    <!-- HTML -->
                    <text_html>
                        <type>text/html</type>
                        <model>mageflow_connect/api2_renderer_html</model>
                    </text_html>
                </renders>
            </response>
        </api2>
    </global>

    <admin>
        <routers>
            <mageflow_connect>
                <use>admin</use>
                <args>
                    <module>Mageflow_Connect</module>
                    <frontName>mageflow_connect</frontName>
                </args>
            </mageflow_connect>
        </routers>
    </admin>
    <adminhtml>
        <layout>
            <updates>
                <mageflow_connect>
                    <file>mageflow/connect/layout.xml</file>
                </mageflow_connect>
            </updates>
        </layout>
    </adminhtml>
    <frontend>
        <layout>
            <updates>
                <mageflow_connect>
                    <file>mageflow/connect/layout.xml</file>
                </mageflow_connect>
            </updates>
        </layout>
    </frontend>
    <default>
        <mageflow_connect>
            <api>
                <enabled>1</enabled>
                <instance_type>community</instance_type>
            </api>
            <advanced>
                <log_enabled>1</log_enabled>
                <api_url>https://app.mageflow.com/api/1.0/</api_url>
                <log_lines>100</log_lines>
                <log_level>7</log_level>
                <pull_days_back>30</pull_days_back>
            </advanced>
            <system>
                <maintenance_mode>0</maintenance_mode>
            </system>
        </mageflow_connect>
    </default>
    <mageflow>
        <settings>
            <ground_rules>https://app.mageflow.com/groundrules</ground_rules>
            <connect_url>https://app.mageflow.com/secure/connect</connect_url>
            <signup_url>https://app.mageflow.com/</signup_url>
        </settings>
    </mageflow>
    <crontab>
        <jobs>
            <mageflow_update_item_cache>
                <schedule>
                    <cron_expr>0 */1 * * *</cron_expr>
                </schedule>
                <run>
                    <model>mageflow_connect/async_itemcacheupdater::run</model>
                </run>
            </mageflow_update_item_cache>
        </jobs>
    </crontab>
</config>
