<?php

/**
 * MageFlow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@mageflow.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * If you wish to use the MageFlow Connect extension as part of a paid
 * service please contact licence@mageflow.com for information about
 * obtaining an appropriate licence.
 */

/**
 * AjaxController.php
 *
 * PHP version 5
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Controller
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */

/**
 * Mageflow_Connect_AjaxController
 *
 * @category   MFX
 * @package    Mageflow_Connect
 * @subpackage Controller
 * @author     MageFlow OÜ, Estonia <info@mageflow.com>
 * @copyright  Copyright (C) 2014 MageFlow OÜ, Estonia (http://mageflow.com) 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link       http://mageflow.com/
 */
class Mageflow_Connect_AjaxController
    extends Mageflow_Connect_Controller_AbstractController
{

    /**
     * test connection to MF API
     */
    public function testapiAction()
    {
        $this->log('testing');
        $client = $this->getApiClient();
        $instanceKey = Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_INSTANCE_KEY);
        $response = json_decode($client->get('whoami', array('instance_key' => $instanceKey)));
        $this->log($response);
        $jsonData = Mage::helper('core')->jsonEncode($response);
        $this->getResponse()->setHeader('Content-Type', 'application/json', true);
        $this->getResponse()->setBody($jsonData);
    }

    /**
     * Generate unique token and save it in session for
     * later reference
     */
    public function getTokenAction()
    {
        $helper = Mage::helper('mageflow_connect');

        $connectUrl = $helper->getSettingValue(Mageflow_Connect_Model_System_Config::CONNECT_URL);

        $this->log('Using connect URL: ' . $connectUrl);

        $response = new stdClass();

        $response->token = Mage::helper('mageflow_connect')->randomHash(32);

        $response->callbackUrl = Mage::helper('adminhtml')->getUrl(
            'mageflow_connect/connect/connect'
        );

        $response->redirectUrl = $connectUrl;

        $response->comehomeUrl = Mage::helper('adminhtml')->getUrl('system_config');

        $response->instanceKey = Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_INSTANCE_KEY);

        $response->instanceType = Mageflow_Connect_Model_System_Config::INSTANCETYPE_CE;

        $type = Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_INSTANCE_TYPE);

        if (!is_null($type)) {
            $response->instanceType = $type;
        }

        Mage::getModel('admin/session')->setMfToken($response->token);

        $jsonData = Mage::helper('core')->jsonEncode($response);

        $this->log('Response JSON: ' . $jsonData);

        $this->getResponse()->setHeader('Content-Type', 'application/json', true);
        return $this->getResponse()->setBody($jsonData);
    }

    public function toggleMonitorAction()
    {
        $this->log('toggle instance monitor status');
        $client = $this->getApiClient();
        $instanceKey = Mage::app()->getStore()->getConfig(Mageflow_Connect_Model_System_Config::API_INSTANCE_KEY);
        $json = $client->get(sprintf('instance/%s/toggle_monitor', $instanceKey));
        $this->getResponse()->setHeader('Content-Type', 'application/json', true);
        return $this->getResponse()->setBody($json);
    }
}
