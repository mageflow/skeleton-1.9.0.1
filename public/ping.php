<?php

/**
 *
 * ping.php is a script that responds to ping information requests about
 * server performance.
 * It is installed to every server that's using MageFlow.
 * It is called regularly by a recurring queue job.
 * This script supports PHP 5.4
 *
 * @author  Sven Varkel <sven@mageflow.com>
 * @license MageFlow EULA
 */
define('SERVER_KEY_FILE_PATH', '/srv/server-status/.mageflow_server_key');

class MageFlowChecker
{

    private $debugEnabled = false;

    public function run()
    {
        $os = strtolower(php_uname('s'));
        switch ($os) {
            case 'darwin':
                $cmd = '/usr/sbin/sysctl -n hw.ncpu';
                break;
            default:
                $cmd = '/usr/bin/nproc';
        }
        $this->debug($cmd);
        $coreCount = (int)shell_exec($cmd);
        //set core count to 1 if it's not detected. It must
        //not and cannot be 0 because every machine has a cpu...;)
        if ($coreCount == 0) {
            $coreCount = 1;
        }
        $this->debug($coreCount);
        $load = sys_getloadavg();
        $this->debug($load);
        $balancedLoad = round($load[1] / $coreCount, 4);
        $this->debug($balancedLoad);

        $this->log($load, __METHOD__, __LINE__);

        //here we post nr of cores and current load to MageFlow REST API
        if (file_exists(SERVER_KEY_FILE_PATH)) {
            $serverKey = file_get_contents(SERVER_KEY_FILE_PATH);
            $data = array(
                'server_key' => trim($serverKey),
                'numproc' => $coreCount,
                'load_avg' => round($balancedLoad, 4),
                'load_1' => round($load[0], 4),
                'load_2' => round($load[1], 4),
                'load_3' => round($load[2], 4),
                'timestamp' => time(),
            );
            if (null !== $this->getMagentoLastAccessTime()) {
                $data['last_access_time'] = $this->getMagentoLastAccessTime();
            }
            $out = implode('|', $data);
            ob_end_clean();
            header('Content-Type: text/plain', true);
            header('Content-Length: ' . strlen($out), true);
            echo $out;
        }
    }

    /**
     * Gets last access time
     */
    private function getMagentoLastAccessTime()
    {
        if (include_once dirname(__FILE__) . '/app/Mage.php') {
            Mage::app();
            $helper = Mage::helper('mageflow_connect');
            if ($helper instanceof Mageflow_Connect_Helper_Data) {
                return $helper->getLastAccessTime();
            }
        }
        return null;
    }

    /**
     * Debug helper
     *
     * @param $var
     */
    private function debug($var)
    {
        if ($this->debugEnabled) {
            if (function_exists('debug_backtrace')) {
                $trace = debug_backtrace();
                printf(
                    "%s\n%s(%s):\n%s\n", str_repeat('-', 80), $trace[1]['function'], $trace[0]['line'],
                    print_r($var, true)
                );
            }
        }
    }

    /**
     * Logs line
     *
     * @param        $var
     * @param string $method
     * @param int $line
     */
    private function log($var, $method = null, $line = null)
    {
        $logDir = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'logs';
        $logFile = $logDir . DIRECTORY_SEPARATOR . basename(__FILE__) . '.log';

        if (!file_exists($logDir)) {
            mkdir($logDir, 0777, true);
        }
        $f = fopen($logFile, 'a+');

        if (is_null($method) && is_null($line) && function_exists('debug_backtrace')) {
            $trace = debug_backtrace();
            $method = $trace[1]['function'];
            $line = $trace[0]['line'];
        }
        $logLine = sprintf("%s\t%s(%s): %s\n", date('c'), $method, $line, print_r($var, true));
        if (is_resource($f)) {
            if (flock($f, LOCK_EX)) {
                fwrite($f, $logLine);
            }
            fclose($f);
        }
    }

}

$worker = new MageFlowChecker();

//catch all exceptions and
//e-mail these to MageFlow Operations
try {
    $worker->run();
} catch (Exception $e) {

    $serverKey = 'N/A';
    if (file_exists($keyFile)) {
        $serverKey = file_get_contents($keyFile);
    }
    $hostname = 'N/A';
    if (function_exists('gethostname')) {
        $hostname = gethostname();
    }
    $line = $e->getLine();
    $file = $e->getFile();
    $code = $e->getCode();
    $message = $e->getMessage();
    $trace = $e->getTraceAsString();
    $from = sprintf('mageflow@%s', $hostname);
    $exceptionMessage = <<<MESSAGE
An exception occurred in server $serverKey while running mageflow_server_checker.php.

Exception details follow:
 FILE: $file
 LINE: $line
 CODE: $code
 MESSAGE: $message
 STACKTRACE: $trace
MESSAGE;

    $headers = array(
        "From: $from"
    );
    error_log($exceptionMessage, 1, 'exceptions@mageflow.com', implode("\r\n", $headers));
}
